"use strict";
let express = require("express");
var path = require('path');
let chat_app = require('express')();
let http = require('http').Server(chat_app);
let io = require('socket.io')(http);
let clientListNames = [];

chat_app.use(express.static(__dirname, '/'));
chat_app.use(express.static(__dirname, '/server/'));
chat_app.use(express.static(__dirname + "/..", '/client/'));
chat_app.use(express.static(__dirname + '/node_modules'));

chat_app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});


chat_app.get('/chat', function(req, res){
	res.redirect('/');
});

chat_app.get('/chat/*', function(req, res){
	res.redirect('/');
});


io.on('connection', function(socket){

	let user = {
		"nickname": socket.handshake.query.userName,
		"color": socket.handshake.query.color,
		"room": socket.handshake.query.room
	}


	clientListNames.push(user);

	io.emit("updateSocketList", clientListNames);
	io.emit("addUserToSocketList", user);
	
	socket.on('disconnect', function(){
		let name=socket.handshake.query.userName;
		let userIndex = -1;
		for(var i = 0, len = clientListNames.length; i < len; i++) {
			if (clientListNames[i].nickname === name) {
				userIndex = i;
				break;
			}
		}

		if (userIndex != -1) {
		clientListNames.splice(userIndex, 1);
		io.emit("updateSocketList", clientListNames);
		io.emit("removeUserFromSocketList",name);
		}
  	});

	socket.on('chatMessageToSocketServer', function(msg, func){
		func("Message recieved!",socket.handshake.query.userName, socket.handshake.query.room);

		let nickname = socket.handshake.query.userName;
		let color = socket.handshake.query.color;
		let room = socket.handshake.query.room;

		let sockectObj = {room, nickname, color, msg}
		io.emit('broadcastToAll_chatMessage', sockectObj);
	});

	socket.on('chatChangeRoom', function(room){
		socket.handshake.query.room = room;

		let name=socket.handshake.query.userName;
		let userIndex = -1;

		for(var i = 0, len = clientListNames.length; i < len; i++) {
			if (clientListNames[i].nickname === name) {
				clientListNames[i].room = room;
			}
		}
		io.emit("updateSocketList", clientListNames);

	});
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
