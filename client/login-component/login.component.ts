import { Component } from "@angular/core";
import { Router, ActivatedRoute }    from "@angular/router";
import * as globalVars from "../service/global";
import {Inject} from "@angular/core";
import { ColorPickerService } from 'angular2-color-picker';

import { AppSettings } from '../app.settings';

import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';

/// <reference path="../../typings/globals/jquery/index.d.ts/>

import "/socket.io/socket.io.js";

import {User} from "../model/user";

@Component({
    moduleId: module.id,
    selector: "nick-name",
    templateUrl: "login.component.html"
})

export class LoginComponent {
    nickname: string = null;
    private color: string = "#127bdc";
    protected router;
    public rooms;

    public user = new User('', '', 'General'); // our form model

    constructor( @Inject(Router) router: Router,
                 private cpService: ColorPickerService,
                 public activatedRoute: ActivatedRoute,
                 private formBuilder: FormBuilder) {

        this.router = router;
        this.rooms = AppSettings.CHATROOMS;
    }

    onSubmit() {

        this.user.color = this.color;
        if (this.user.nickname) {
            globalVars.socket = io({ query: "room="+this.user.room + "&userName=" + this.user.nickname + "&color=" + this.user.color });
            this.router.navigate(["/chat", this.user.room]);
        }
    }


}