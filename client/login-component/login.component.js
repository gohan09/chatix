"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var globalVars = require("../service/global");
var core_2 = require("@angular/core");
var app_settings_1 = require("../app.settings");
/// <reference path="../../typings/globals/jquery/index.d.ts/>
require("/socket.io/socket.io.js");
var user_1 = require("../model/user");
var LoginComponent = (function () {
    function LoginComponent(router, cpService, activatedRoute, formBuilder) {
        this.cpService = cpService;
        this.activatedRoute = activatedRoute;
        this.formBuilder = formBuilder;
        this.nickname = null;
        this.color = "#127bdc";
        this.user = new user_1.User('', '', 'General'); // our form model
        this.router = router;
        this.rooms = app_settings_1.AppSettings.CHATROOMS;
    }
    LoginComponent.prototype.onSubmit = function () {
        this.user.color = this.color;
        if (this.user.nickname) {
            globalVars.socket = io({ query: "room=" + this.user.room + "&userName=" + this.user.nickname + "&color=" + this.user.color });
            this.router.navigate(["/chat", this.user.room]);
        }
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "nick-name",
        templateUrl: "login.component.html"
    }),
    __param(0, core_2.Inject(router_1.Router))
], LoginComponent);
exports.LoginComponent = LoginComponent;
