import { NgModule }      from "@angular/core";
import { RouterModule }	from "@angular/router";
import { CommonModule } from "@angular/common";
import { LoginComponent }  from "./login.component";
import { ColorPickerModule } from 'angular2-color-picker';
import { FormsModule, ReactiveFormsModule }   from "@angular/forms";

@NgModule({
    imports:      [ CommonModule, FormsModule, RouterModule, ColorPickerModule, ReactiveFormsModule],
    declarations: [ LoginComponent ],
    exports: 	  [ LoginComponent ]
})

export class LoginModule {}
