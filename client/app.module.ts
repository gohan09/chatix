import { NgModule }      from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";


import { appRoutes } from "./app.routing";
import { AppComponent } from "./app.component";

import { routing, appRoutingProviders } from "./app.routing";


import { LoginModule }  from "./login-component/index";
import { ChatModule }  from "./chat-component/index";


@NgModule({
  imports:      [ BrowserModule, routing, RouterModule, RouterModule.forRoot(appRoutes), LoginModule, ChatModule],
  declarations: [ AppComponent ],
  providers: 	[ appRoutingProviders ],
  bootstrap:    [ AppComponent ]
})

export class AppModule { }
