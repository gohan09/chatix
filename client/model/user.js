"use strict";
var User = (function () {
    function User(nickname, color, room) {
        this.nickname = nickname;
        this.color = color;
        this.room = room;
    }
    return User;
}());
exports.User = User;
