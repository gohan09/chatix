export class User {
    constructor(
        public nickname: string,
        public color:string,
        public room: string
    ){}
}
