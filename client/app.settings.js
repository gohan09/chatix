"use strict";
var AppSettings = (function () {
    function AppSettings() {
    }
    Object.defineProperty(AppSettings, "CHATROOMS", {
        get: function () {
            var rooms = [];
            rooms.push({
                key: 'general',
                value: 'General'
            });
            rooms.push({
                key: 'programming',
                value: 'Programming'
            });
            rooms.push({
                key: 'sport',
                value: 'Sport'
            });
            return rooms;
        },
        enumerable: true,
        configurable: true
    });
    return AppSettings;
}());
exports.AppSettings = AppSettings;
