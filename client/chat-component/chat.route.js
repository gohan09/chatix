"use strict";
var chat_component_1 = require("./chat.component");
exports.chatComponentRoutes = [
    { path: 'chat/:selectedRoom', component: chat_component_1.ChatComponent },
    { path: 'chat', component: chat_component_1.ChatComponent }
];
