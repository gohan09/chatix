import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChatComponent } from "./chat.component";
import {BrowserModule} from '@angular/platform-browser';
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule }   from "@angular/forms";


@NgModule({
    imports: [BrowserModule, CommonModule, RouterModule, FormsModule],
    declarations: [ChatComponent],
    exports: [ChatComponent]
})

export class ChatModule { }