"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var globalVars = require("../service/global");
var router_1 = require("@angular/router");
var core_2 = require("@angular/core");
var app_settings_1 = require("../app.settings");
/// <reference path="../../typings/globals/jquery/index.d.ts/>
require("/socket.io/socket.io.js");
var ChatComponent = (function () {
    function ChatComponent(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.resFlag = false;
        this.newUser = false;
        this.exitedUser = false;
        this.newUserName = null;
        this.exitedUserName = null;
        this.sentMessageUsername = null;
        this.msgCount = 0;
        this.rooms = app_settings_1.AppSettings.CHATROOMS;
        var urlSegments = router.url.split('/'); //  /routename;
        this.selectedRoom = urlSegments[urlSegments.length - 1];
        var reference = this;
        var temp;
        globalVars.socket.on("broadcastToAll_chatMessage", function (resObj) {
            console.log(resObj.room);
            console.log(reference.selectedRoom);
            if (reference.selectedRoom != resObj.room) {
                return;
            }
            reference.msgCount++;
            var time = new Date().toLocaleTimeString('en-US', { hour12: false,
                hour: "numeric",
                minute: "numeric" });
            if (reference.sentMessageUsername !== resObj.nickname) {
                resObj.nickname = resObj.nickname + ": ";
                temp = $("#messages-list").length;
                console.log(reference.msgCount);
                $("#messages-list").append($("<li data-index=" + reference.msgCount + ">"));
                $("li[data-index=" + reference.msgCount + "]").append($("<div class='left-msg' style='background:" + resObj.color + "' data-index=" + reference.msgCount + ">"));
                $("div[data-index=" + reference.msgCount + "]").append($("<span class='name'>").text(resObj.nickname));
                $("div[data-index=" + reference.msgCount + "]").append($("<span class='msg'>").text(resObj.msg));
            }
            else if (reference.sentMessageUsername === resObj.nickname) {
                $("#messages-list").append($("<li data-index=" + reference.msgCount + ">"));
                $("li[data-index=" + reference.msgCount + "]").append($("<div class='right-msg' style='background:" + resObj.color + "' data-index=" + reference.msgCount + ">"));
                $("div[data-index=" + reference.msgCount + "]").append($("<span class='msg'>").text(resObj.msg));
                reference.sentMessageUsername = null;
            }
            $("div[data-index=" + reference.msgCount + "]").append($("<span class='datetime'>").text(time));
        });
        globalVars.socket.on("updateSocketList", function (list) {
            var data = [];
            console.log(list);
            for (var _i = 0, list_1 = list; _i < list_1.length; _i++) {
                var entry = list_1[_i];
                if (entry.room == reference.selectedRoom) {
                    data.push(entry);
                }
            }
            reference.clientsNameList = data;
        });
        globalVars.socket.on("addUserToSocketList", function (user) {
            if (user.room == reference.selectedRoom) {
                reference.exitedUser = false;
                reference.newUser = true;
                reference.newUserName = user.nickname;
            }
        });
        globalVars.socket.on("removeUserFromSocketList", function (username) {
            reference.newUser = false;
            reference.exitedUser = true;
            reference.exitedUserName = username;
            reference.clientsNameList = [];
        });
    }
    ChatComponent.prototype.updateSocketList = function (list) {
        var data = [];
        for (var _i = 0, list_2 = list; _i < list_2.length; _i++) {
            var entry = list_2[_i];
            if (entry.room == this.selectedRoom) {
                data.push(entry);
            }
        }
        this.clientsNameList = data;
    };
    ChatComponent.prototype.sendMessage = function (data) {
        this.resFlag = true;
        var reference = this;
        globalVars.socket.emit("chatMessageToSocketServer", data.value, function (respMsg, userName, room) {
            reference.sentMessageUsername = userName;
            reference.response = respMsg;
            reference.sentRoom = room;
        });
        $("#message-boxID").val(" ");
    };
    ChatComponent.prototype.sendMessageOnEnter = function ($event, message) {
        if ($event.which === 13) {
            this.sendMessage(message);
        }
    };
    ChatComponent.prototype.changeRoom = function () {
        globalVars.socket.emit("chatChangeRoom", this.selectedRoom);
        this.updateSocketList(this.clientsNameList);
    };
    ChatComponent.prototype.update = function () {
        this.resFlag = false;
        this.newUser = false;
        this.exitedUser = false;
    };
    return ChatComponent;
}());
ChatComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "chat-page",
        templateUrl: "./chat.component.html",
        styleUrls: ['./chat.component.css'],
        encapsulation: core_1.ViewEncapsulation.None
    }),
    __param(0, core_2.Inject(router_1.Router))
], ChatComponent);
exports.ChatComponent = ChatComponent;
