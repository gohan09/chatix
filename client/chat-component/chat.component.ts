import { Component, ViewEncapsulation, OnInit, OnDestroy } from "@angular/core";
import * as globalVars from "../service/global";
import { Router, ActivatedRoute }    from "@angular/router";
import { Inject } from "@angular/core";
import { Validators, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { AppSettings } from '../app.settings';

import { User } from '../model/user';


import { ActivatedRoute } from '@angular/router';

/// <reference path="../../typings/globals/jquery/index.d.ts/>


import "/socket.io/socket.io.js";





@Component({
    moduleId: module.id,
    selector: "chat-page",
    templateUrl: "./chat.component.html",
    styleUrls:  ['./chat.component.css'],
    encapsulation: ViewEncapsulation.None,
})

export class ChatComponent implements OnInit, OnDestroy {
    reference: any;
    resFlag: boolean = false;
    newUser: boolean = false;
    exitedUser: boolean = false;
    newUserName: string = null;
    exitedUserName: string = null;
    sentMessageUsername: string = null;
    response: string;
    clientsNameList: User[];
    sentRoom: string;
    message: string;
    msgCount: number = 0;
    rooms: any;


    selectedRoom: string;


    constructor(
        @Inject(Router) public router: Router,
        public activatedRoute: ActivatedRoute,
    ) {
        this.rooms = AppSettings.CHATROOMS;

        let urlSegments = router.url.split('/') //  /routename;
        this.selectedRoom = urlSegments[urlSegments.length-1];

        let reference = this;
        let temp;

        globalVars.socket.on("broadcastToAll_chatMessage", function(resObj) {


            console.log(resObj.room);
            console.log(reference.selectedRoom);

            if(reference.selectedRoom != resObj.room){
                return;
            }

            reference.msgCount++;

            let time = new Date().toLocaleTimeString('en-US', { hour12: false,
                hour: "numeric",
                minute: "numeric"});

            if (reference.sentMessageUsername !== resObj.nickname) {
                resObj.nickname = resObj.nickname + ": ";

                temp = $("#messages-list").length;

                console.log(reference.msgCount);
                $("#messages-list").append($("<li data-index=" + reference.msgCount + ">"));

                $("li[data-index=" + reference.msgCount + "]").append($("<div class='left-msg' style='background:"+ resObj.color +"' data-index=" + reference.msgCount + ">"));
                $("div[data-index=" + reference.msgCount + "]").append($("<span class='name'>").text(resObj.nickname));
                $("div[data-index=" + reference.msgCount + "]").append($("<span class='msg'>").text(resObj.msg));

            }
            else if (reference.sentMessageUsername === resObj.nickname) {
                $("#messages-list").append($("<li data-index=" + reference.msgCount + ">"));
                $("li[data-index=" + reference.msgCount + "]").append($("<div class='right-msg' style='background:"+ resObj.color +"' data-index=" + reference.msgCount + ">"));
                $("div[data-index=" + reference.msgCount + "]").append($("<span class='msg'>").text(resObj.msg));
                reference.sentMessageUsername = null;
            }
            $("div[data-index=" + reference.msgCount + "]").append($("<span class='datetime'>").text(time));


        });

        globalVars.socket.on("updateSocketList", function(list){
            let data = [];
            console.log(list);
            for(let entry of list){
                if(entry.room == reference.selectedRoom) {
                    data.push(entry);
                }
            }
            reference.clientsNameList = data;
        });

        globalVars.socket.on("addUserToSocketList", function(user){
            if(user.room == reference.selectedRoom) {
                reference.exitedUser = false;
                reference.newUser = true;
                reference.newUserName = user.nickname;
            }
        });

        globalVars.socket.on("removeUserFromSocketList", function(username){
            reference.newUser = false;
            reference.exitedUser = true;
            reference.exitedUserName = username;
            reference.clientsNameList = [];
        });
    }

    updateSocketList(list) {
        let data = [];
        for(let entry of list){
            if(entry.room == this.selectedRoom) {
                data.push(entry);
            }
        }
        this.clientsNameList = data;
    }


    sendMessage(data) {
        this.resFlag = true;
        let reference = this;

        globalVars.socket.emit("chatMessageToSocketServer", data.value, function(respMsg, userName, room){
            reference.sentMessageUsername = userName;
            reference.response = respMsg;
            reference.sentRoom = room;
        });
        $("#message-boxID").val(" ");
    }

    sendMessageOnEnter($event, message) {
        if ($event.which === 13) { // ENTER_KEY
            this.sendMessage(message);
        }
    }


    changeRoom() {
        globalVars.socket.emit("chatChangeRoom", this.selectedRoom);
        this.updateSocketList(this.clientsNameList);
    }

    update() {
        this.resFlag = false;
        this.newUser = false;
        this.exitedUser = false;
    }



}