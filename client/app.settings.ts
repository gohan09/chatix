
export class AppSettings {
  public static get CHATROOMS(): any {

    let rooms = [];
    rooms.push({
      key: 'general',
      value: 'General'
    })
    rooms.push({
      key: 'programming',
      value: 'Programming'
    })
    rooms.push({
      key: 'sport',
      value: 'Sport'
    })


    return rooms;

  }
}
